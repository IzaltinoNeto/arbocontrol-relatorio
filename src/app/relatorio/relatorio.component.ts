
import { Component, OnInit, Input } from '@angular/core';
import { RelatorioService } from './relatorio.service';
import { WebDataRocksPivot } from '../webdatarocks/webdatarocks.angular4';
import { template } from '@angular/core/src/render3';
import * as localização from '../../BrazilianPortuguese.json';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {
  //guarda os dados que serao utilizados no relatorio
  dadosF= [];
  relatorioService : RelatorioService;
  //flag utilizada para controlar se o componente será inicializado na tela ou nao
  iniciarComponente = false;
  constructor(relatorioService : RelatorioService) { 
    this.relatorioService = relatorioService;
    this.dadosF = this.relatorioService.dataReport;
   }
  
  ngOnInit() {
      console.log("localização: "+JSON.stringify(localização.default));
    //requisicao dos dados via http
    this.relatorioService.getRelatorios(this);
}


  //retorna um JSON com a formatação dos dados no relatório e os dados que serão utilizados
  getReport() { 

    return {
      dataSource: {
          //coloca os dados do relatorio dentro do JSON
        data:this.dadosF
      },
      //formatação do relatório(colunas, linhas, filtros...) 
      "slice": {
        "reportFilters": [
            {
                "uniqueName": "Data.Year"
            },
            {
                "uniqueName": "Data.Month"
            },
            {
                "uniqueName": "Data.Day"
            }
        ],
        "rows": [
            {
                "uniqueName": "Supervisor"
            },
            {
                "uniqueName": "Agente"
            }
        ],
        "columns": [
            {
                "uniqueName": "Measures"
            }
        ],
        "measures": [
            {
                "uniqueName": "A1",
                "aggregation": "sum"
            },
            {
                "uniqueName": "A2",
                "aggregation": "sum"
            },
            {
                "uniqueName": "B",
                "aggregation": "sum"
            },
            {
                "uniqueName": "C",
                "aggregation": "sum"
            },
            {
                "uniqueName": "CheckList Implantado",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Colher Pequena",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Colher Grande",
                "aggregation": "sum"
            },
            {
                "uniqueName": "D1",
                "aggregation": "sum"
            },
            {
                "uniqueName": "D2",
                "aggregation": "sum"
            },
            {
                "uniqueName": "E",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Imóvel Positivo",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Imovel Tratado",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Total Eliminado",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Total Inspecionado",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Total Positivo",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Total Tratado",
                "aggregation": "sum"
            },
            {
                "uniqueName": "Tubitos",
                "aggregation": "sum"
            }
        ],
        
    },
    "options": {
        "grid": {
            "type": "classic"
        },
        "showAggregationLabels": false
    },
    "localization": localização.default
}
  }
  
  
     
}

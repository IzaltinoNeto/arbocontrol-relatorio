import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable()
export class RelatorioService{
    dataReport = [];
    baseURL: string;

    constructor(private http:HttpClient){
        this.baseURL = 'https://gleytonlima.com:9998/api/v1/atividades/ordem-servico/5bdfa8f2139059769f9146c8?page=0&size=100&formularioId=5bdfa86e139059769f9146c6';
    }

    getRelatorios(component){

       this.http.get(this.baseURL)
       .subscribe((data: any) => {console.log("entrou na requisição");
                                    //montar um vetor de objetos com os dados necessarios
                                    data.content.forEach((element,index) => {
                                       
                                        
                                        this.dataReport.push({
                                            Agente : element.respostas.equipe_agente ,
                                            A1 : element.respostas.deposito_a1,
                                            A2 : element.respostas.deposito_a2,
                                            B : element.respostas.deposito_b,
                                            C : element.respostas.deposito_c,
                                            D1 : element.respostas.deposito_d1,
                                            D2 : element.respostas.deposito_d2,
                                            E : element.respostas.deposito_e,
                                            'Total Inspecionado' : element.respostas.deposito_total_inspecionado,
                                            'Total Positivo' : element.respostas.deposito_total_positivo,
                                            'Total Eliminado' : element.respostas.deposito_total_eliminado,
                                            'Total Tratado' : element.respostas.deposito_tratado,
                                            'Colher Pequena' : element.respostas.tratamento_larvicida_colher_pequena,
                                            'Colher Grande' : element.respostas.tratamento_larvicida_colher_grande,
                                            'CheckList Implantado' : element.respostas.tratamento_aderiu_checklist = true ? 1: 0,
                                            'Imovel Tratado' : element.respostas.deposito_tratado>0 ? 1 : 0 ,
                                            'Imóvel Positivo': element.respostas.deposito_total_positivo>0 ? 1 : 0 ,
                                            Tubitos : element.respostas.amostras_coletadas_qtde_tubos,
                                            Supervisor : element.respostas.equipe_supervisor,
                                            UF : element.respostas.estado,
                                            Cidade : element.respostas.cidade,
                                            'Distrito': element.respostas.distritos,
                                            'Tipo de Atividade' : element.respostas.atividade_tipo_atividade,
                                            Data: element.respostas.inspecao_data

                                        });
                                    
                                    }
                    );
                    //permite a inicialização do component de relatorio na tela           
                    component.iniciarComponente = true;
                            });

                                    
    }
   
    

    
}
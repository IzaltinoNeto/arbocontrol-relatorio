import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { WebDataRocksPivot } from "./webdatarocks/webdatarocks.angular4";
import { AppComponent } from './app.component';
import { RelatorioComponent } from './relatorio/relatorio.component';
import {RelatorioService} from './relatorio/relatorio.service';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    RelatorioComponent,
    WebDataRocksPivot
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [RelatorioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
